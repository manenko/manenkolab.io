<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style type="text/css">


    body {
      font-family: monospace;
      display: flex;
      justify-content: center;
      font-size: 0.9em;
      line-height: 1.5;
      background: #fefefe;
      color: #111;
    }

    p {
      text-align: justify;
    }

    article {
      width: 80ch;
    }

    pre {
      overflow-x: auto;
      background: #efefef;
      padding: 4px;
    }

    code {
      font-weight: bold;
    }

    div.figure pre {
      line-height: 1.0;
      margin-bottom: 2px;
    }

    div.figure span.caption {
      font-weight: normal;
      text-align: center;
      display: inline-block;
      width: 100%;
    }

    h1 {
      text-align: center;
    }

    ol {
      list-style-type: lower-latin;
    }
  </style>
  <title>Random thoughts</title>
</head>
<body>
<article>
<!--Category: c,c++,programming,utf-8,unicode
  Author: Oleksandr Manenko
    Date: 2022-07-11-->


<h1>Sparse sets</h1>
<h2>Introduction</h2>
<p>A sparse set is a simple data structure that has a few interesting properties:</p>
<ol>
    <li><i>O(1)</i> to add an item.</li>
    <li><i>O(1)</i> to remove an item.</li>
    <li><i>O(1)</i> to lookup an item.</li>
    <li><i>O(1)</i> to clear the set.</li>
    <li><i>O(n)</i> to iterate over the set.</li>
</ol>
<p>A set does not require its internal items storage to be initialised upon creation (!).</p>
<p>Sparse sets are commonly used to implement <a href="https://en.wikipedia.org/wiki/Entity_component_system">Entity Component System</a> architectural pattern. I plan to cover ECS in one of my future posts but for now let’s try to understand and implement sparse sets.</p>
<h2>Overview</h2>
<p>Sparse sets use two integer arrays internally: <code>dense</code> and <code>sparse</code>. The former is a packed array that stores the set’s items (integers) in the insertion order. The latter is an array that can have holes (hence the name – <code>sparse</code>) and it maps the set’s items to their indices in <code>dense</code>. The set also keeps track of how many items it has. We call it <code>N</code>.</p>
<p>Sparse sets could be implemented as growable, i.e. being able to reallocate the memory they use but I will use non-growable version in this post for simplicity. This means a user has to specify the size of the set upon creation.</p>
<pre>
/** An item stored in a sparse set. */
typedef uint32_t rho_ss_id;

/** Maximum possible value of the rho_ss_id. */
static const rho_ss_id rho_ss_id_max = UINT32_MAX - 1;

/** The sparse set. */
struct rho_ss {
    rho_ss_id *sparse;  /**< Sparse array used to speed-optimise the set. */
    rho_ss_id *dense;   /**< Dense array that stores the set's items.     */
    rho_ss_id  n;       /**< Number of items in the dense array.          */
    rho_ss_id  max;     /**< Maximum allowed item ID.                     */
};
</pre>
<h2>An empty set</h2>
<p>We start with an empty set that can have up to <code>max</code> items. The next picture demonstrates the sparse set that can hold up to ten items.</p>
<div class="figure">
<pre>
  N                                                   
  │                                                   
  │                                                   
  │                                                   
  ▼                                                   
  0   1   2   3   4   5   6   7   8   9               
┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┐             
│   │   │   │   │   │   │   │   │   │   │◀──────Dense 
└───┴───┴───┴───┴───┴───┴───┴───┴───┴───┘             
                                                      
┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┐             
│   │   │   │   │   │   │   │   │   │   │◀──────Sparse
└───┴───┴───┴───┴───┴───┴───┴───┴───┴───┘             
  0   1   2   3   4   5   6   7   8   9               
</pre>
<span class="caption">Figure 1: A recently created, empty sparse set.</span>
</div>
<p><code>N</code> is 0, <code>dense</code> and <code>sparse</code> are allocated but not initialised.</p>
<pre>
struct rho_ss rho_ss_alloc( rho_ss_id max_id )
{
    assert( max_id > 0 && max_id <= rho_ss_id_max );

    size_t        array_size = sizeof( rho_ss_id ) * ( max_id + 1 );
    struct rho_ss ss         = { 0 };

    ss.dense  = malloc( array_size );
    ss.sparse = malloc( array_size );

    if ( !ss.dense || !ss.sparse ) {
        free( ss.dense );
        free( ss.sparse );

        ss.dense  = NULL;
        ss.sparse = NULL;
    } else {
        ss.max = max_id;
    }

    return ss;
}
</pre>
<h2>Adding a first item</h2>
<p>Let's add 4 as a first item to the set.</p>
<div class="figure">
<pre>
      N                                               
      │                                               
      │                                               
      │                                               
      ▼                                               
  0   1   2   3   4   5   6   7   8   9               
┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┐             
│ 4 │   │   │   │   │   │   │   │   │   │◀──────Dense 
└───┴───┴───┴───┴───┴───┴───┴───┴───┴───┘             
  ▲                                                   
  │                                                   
  └───────────────┐                                   
                  │                                   
                  ▼                                   
┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┐             
│   │   │   │   │ 0 │   │   │   │   │   │◀──────Sparse
└───┴───┴───┴───┴───┴───┴───┴───┴───┴───┘             
  0   1   2   3   4   5   6   7   8   9               
</pre>
<span class="caption">Figure 2: Sparse set with a first element added.</span>
</div>
<p>First, we add the item to the <code>dense</code> at index 0 (this is the current value of <code>N</code>). Then we write <code>N</code> to the sparse array at index 4. Now both slots in the <code>dense</code> and <code>sparse</code> arrays point to each other. Lastly, we increase <code>N</code> by one.</p>
<h2>Adding a second item</h2>
<p>Let’s add 6 as a second item.</p>
<div class="figure">
<pre>
          N                                           
          │                                           
          │                                           
          │                                           
          ▼                                           
  0   1   2   3   4   5   6   7   8   9               
┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┐             
│ 4 │ 6 │   │   │   │   │   │   │   │   │◀──────Dense 
└───┴───┴───┴───┴───┴───┴───┴───┴───┴───┘             
  ▲   ▲                                               
  │   └───────────────────┐                           
  └───────────────┐       │                           
                  │       │                           
                  ▼       ▼                           
┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┐             
│   │   │   │   │ 0 │   │ 1 │   │   │   │◀──────Sparse
└───┴───┴───┴───┴───┴───┴───┴───┴───┴───┘             
  0   1   2   3   4   5   6   7   8   9               
</pre>
<span class="caption">Figure 3: Sparse set with a second element added.</span>
</div>
<p>The steps are the same. We put 6 to the next free slot of the <code>dense</code> array, write <code>N</code> to the <code>sparse</code> at index 6, and increment <code>N</code> by one.</p>
<h2>Adding a third item</h2>
<p>Now we add 0 as a third item.</p>
<div class="figure">
<pre>
              N                                       
              │                                       
              │                                       
              │                                       
              ▼                                       
  0   1   2   3   4   5   6   7   8   9               
┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┐             
│ 4 │ 6 │ 0 │   │   │   │   │   │   │   │◀──────Dense 
└───┴───┴───┴───┴───┴───┴───┴───┴───┴───┘             
  ▲   ▲   ▲                                           
  │   └───│───────────────┐                           
  └───────│───────┐       │                           
  ┌───────┘       │       │                           
  ▼               ▼       ▼                           
┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┐             
│ 2 │   │   │   │ 0 │   │ 1 │   │   │   │◀──────Sparse
└───┴───┴───┴───┴───┴───┴───┴───┴───┴───┘             
  0   1   2   3   4   5   6   7   8   9               
</pre>
<span class="caption">Figure 4: Sparse set with a third element added.</span>
</div>
<p>There is nothing new here. The item we added is 0 so we put its <code>dense</code> index (which is 2) to <code>sparse</code> at index 0.</p>
<p>And this is how we implement it:</p>
<pre>
void rho_ss_add( struct rho_ss *ss, rho_ss_id id )
{
    assert( ss );
    assert( id <= ss->max );

    if ( rho_ss_has( ss, id ) ) {
        return;
    }

    ss->dense[ss->n] = id;
    ss->sparse[id]   = ss->n;

    ++ss->n;
}
</pre>
<p>The <code>rho_ss_has</code> function checks if the given item is in the sparse set. We implement it in the next section.</p>
<h2>Checking whether an item is in the set</h2>
<p>Let's test if 6 is in the set. To do so we go to the <code>sparse</code> array and check its value at index 6. The value is 1. Now we use this value as an index in the <code>dense</code> array. It has value 6 at this index, which means the set contains item 6.</p>
<p>Now let's check if 9 is in the set. We go to the <code>sparse</code> array and check its value at index 9. The value is garbage (we haven't initialised the memory, remember?) and can be anything, let’s say it is <code>G</code>.</p>
<p>If <code>G >= N</code> then <code>G</code> is out of bounds index for the <code>dense</code> array and 9 is not in the set.</p>
<p>Suppose <code>G</code> is less than <code>N</code>. In our case it should be either of 0, 1, or 2 because <code>N</code> is 3. We check if the <code>dense</code> array's value at index <code>G</code> is 9. It is not and it cannot be, so the answer is no, the set does not contain item 9.</p>
<p>Now we can implement the operation:</p>
<pre>
bool rho_ss_has( struct rho_ss *ss, rho_ss_id id )
{
    assert( ss );
    assert( id <= ss->max );

    rho_ss_id dense_index = ss->sparse[id];

    return dense_index < ss->n && ss->dense[dense_index] == id;
}
</pre>
<h2>Removing an item</h2>
<p>Let's remove 6 from the set.</p>
<p>First, we replace 6 with the last item from the <code>dense</code> array, which is 0. Then we have to update the <code>sparse</code> array at index 0 with the new index of 0 in the <code>dense</code> array, which is 1. And then reduce <code>N</code> by one.</p>
<div class="figure">
<pre>
          N                                           
          │                                           
          │                                           
          │                                           
          ▼                                           
  0   1   2   3   4   5   6   7   8   9               
┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┐             
│ 4 │ 0 │ 0 │   │   │   │   │   │   │   │◀──────Dense 
└───┴───┴───┴───┴───┴───┴───┴───┴───┴───┘             
  ▲   ▲                                               
  │   │                                               
  └───│───────────┐                                   
  ┌───┘           │                                   
  ▼               ▼                                   
┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┐             
│ 1 │   │   │   │ 0 │   │ 1 │   │   │   │◀──────Sparse
└───┴───┴───┴───┴───┴───┴───┴───┴───┴───┘             
  0   1   2   3   4   5   6   7   8   9               
</pre>
<span class="caption">Figure 5: Sparse set with item 6 removed.</span>
</div>
<p>In other words, to remove an item from the set, we replace it with the last item in the <code>dense</code> array and update the corresponding <code>sparse</code> array’s slot to point to the new location.</p>
<p>The implementation is as follows:</p>
<pre>
void rho_ss_remove( struct rho_ss *ss, rho_ss_id id )
{
    assert( ss );
    assert( id <= ss->max );

    if ( rho_ss_has( ss, id ) ) {
        --ss->n;
        rho_ss_id dense_index  = ss->sparse[id];
        rho_ss_id item         = ss->dense[ss->n];
        ss->dense[dense_index] = item;
        ss->sparse[item]       = dense_index;
    }
}
</pre>
<h2>Iterating over the set</h2>
<p>This one is simple: you iterate over the <code>dense</code> array. Like this:</p>
<pre>
struct rho_ss ss = rho_ss_alloc( 10 );
rho_ss_add( &ss, 4 );
rho_ss_add( &ss, 6 );
rho_ss_add( &ss, 0 );
// Iterate over the dense array.
for ( rho_ss_id i = 0; i < ss.n; ++i ) {
    printf( "%d ", ss.dense[i] );
}
printf( "%s", "\n" );

rho_ss_free( &ss );
</pre>
<h2>Clearing the set</h2>
To clear a sparse set, set its <code>N</code> to 0:
<pre>
void rho_ss_clear( struct rho_ss *ss )
{
    assert( ss );

    ss->n = 0;
}
</pre>
<p>Like I said before, most of the sparse set operations are super fast.</p>
<h2>What’s next?</h2>
<p>You could play with my <a href="https://gitlab.com/manenko/rho-sparse-set/-/blob/master/include/rho/sparse_set.h">implementation</a> of the sparse set or (and this is the way) implement it on your own.</p>
</article>
</body>
</html>
